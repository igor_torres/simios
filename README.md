# Simios

Microsserviço responsável por verificar simios e estatisticas.

Representa um Simio.

## Começando

As instruções apresentadas abaixo servirão de base para que o projeto entre em execução em uma máquina local para fins de desenvolvimento e acessar no ambiente cloud.

### Pré-requisitos

Para que o microsserviço funcione corretamente, será necessário instalar:  

- Maven (a versão utilizada pode ser consultada no arquivo pom.xml);
- DynamoDB;
- Spring Boot;

### Instalando

Passo a passo de execução para que o microsserviço fique em execução e disponível para uso.


- Realizar o clone do projeto (inserir seu usuário):


```
git clone https://gitlab.com/igor_torres/simios.git
```

- Acessar a pasta para realizar o build do projeto:


```
cd simio
```

- Fazer o build do projeto:


```
mvn clean install
```

- Subir a aplicação:


```
mvn spring-boot:run
```
- Acessar url's:

```
- Local:
   - http://localhost:5000/swagger-ui.html
   - http://localhost:5000/simian
   - http://localhost:5000/stats

- Cloud:
   - http://simian-api.eba-cm9ar3gc.sa-east-1.elasticbeanstalk.com/swagger-ui.html
   - http://simian-api.eba-cm9ar3gc.sa-east-1.elasticbeanstalk.com/simian
   - http://simian-api.eba-cm9ar3gc.sa-east-1.elasticbeanstalk.com/stats 
```