package br.com.ml.simian.controller;

import br.com.ml.simian.exception.NotSimianException;
import br.com.ml.simian.exception.SimianException;
import br.com.ml.simian.model.Race;
import br.com.ml.simian.model.SimianStats;
import br.com.ml.simian.service.SimianService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "simian",
        tags = "simian",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RestController
public class SimianController {

    @Autowired
    private SimianService simianService;

    @ApiOperation(value = "Operação responsável por definir se uma sequencia de DNA é simio ou não.",
            nickname = "Simian")
    @PostMapping(path = "/simian")
    public ResponseEntity<String> simian(
            @ApiParam(name = "dna", value = "Representa a sequencia de gene DNA.", required = true)
            @RequestBody Race race) throws SimianException, NotSimianException {

        boolean simian = simianService.isSimian(race.getDna());

        if(simian){
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            throw new NotSimianException();
        }

    }

    @ApiOperation(value = "Operação responsável por devolver estatistica de verificações de dna.",
            nickname = "Simian")
    @GetMapping(path = "/stats")
    public SimianStats stats() {
        SimianStats stats = simianService.calculateStats();
        return stats;
    }

}
