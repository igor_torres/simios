package br.com.ml.simian.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class SimianControllerAdvice {

    @ExceptionHandler(NotSimianException.class)
    public ResponseEntity<Object> handleNoSimianException(
            NotSimianException ex, WebRequest request) {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(SimianException.class)
    public ResponseEntity<Object> handleSimianException(
            SimianException ex, WebRequest request) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
