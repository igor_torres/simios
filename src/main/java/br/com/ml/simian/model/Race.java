package br.com.ml.simian.model;

import lombok.Data;

@Data
public class Race {

    String[] dna;
}
