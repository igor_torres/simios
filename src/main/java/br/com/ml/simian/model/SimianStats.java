package br.com.ml.simian.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SimianStats {

    Integer count_mutant_dna;
    Integer count_human_dna;
    Double ratio;

}
