package br.com.ml.simian.repository;

import br.com.ml.simian.model.Simian;
import org.springframework.stereotype.Repository;

@Repository
public interface SimianRepository  {

    int count(Simian simian);

    void save(Simian simian);
}
