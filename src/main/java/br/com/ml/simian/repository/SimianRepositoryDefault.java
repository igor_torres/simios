package br.com.ml.simian.repository;

import br.com.ml.simian.model.Simian;
import br.com.ml.simian.repository.config.DynamoDbConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SimianRepositoryDefault implements SimianRepository{

    @Autowired
    DynamoDbConfig dynamoDbConfig;

    @Override
    public int count(Simian simian){
        DynamoDBQueryExpression<Simian> expression = new DynamoDBQueryExpression<Simian>().withHashKeyValues(simian);
        return dynamoDbConfig.dynamoDbMapper().count(Simian.class, expression);
    }

    @Override
    public void save(Simian simian){
        dynamoDbConfig.dynamoDbMapper().save(simian);
    }
}
