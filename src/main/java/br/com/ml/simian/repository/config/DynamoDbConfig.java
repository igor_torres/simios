package br.com.ml.simian.repository.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;

public interface DynamoDbConfig {



    AmazonDynamoDB amazonDynamoDB(AWSCredentialsProvider amazonAwsCredentials);

    AWSCredentialsProvider amazonAwsCredentials();

    DynamoDBMapper dynamoDbMapper();

    DynamoDBMapperConfig dynamoDbMapperConfig();

}
