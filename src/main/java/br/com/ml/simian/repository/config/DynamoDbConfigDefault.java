package br.com.ml.simian.repository.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class DynamoDbConfigDefault implements DynamoDbConfig {

    private String amazonAwsAccessKey = "AKIAILLMSBO2XFGQJJ3A";

    private String amazonAwsSecretKey = "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";

    @Bean
    @Override
    public AmazonDynamoDB amazonDynamoDB(AWSCredentialsProvider amazonAwsCredentials) {

        return AmazonDynamoDBClientBuilder.standard().withCredentials(amazonAwsCredentials).withRegion(Regions.SA_EAST_1).build();

    }

    @Bean
    @Override
    public AWSCredentialsProvider amazonAwsCredentials() {
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(this.amazonAwsAccessKey, this.amazonAwsSecretKey));
    }

    @Bean
    @Override
    public DynamoDBMapper dynamoDbMapper() {
        return new DynamoDBMapper(this.amazonDynamoDB(this.amazonAwsCredentials()), this.dynamoDbMapperConfig());
    }

    @Override
    public DynamoDBMapperConfig dynamoDbMapperConfig() {
        return DynamoDBMapperConfig.DEFAULT;
    }

}
