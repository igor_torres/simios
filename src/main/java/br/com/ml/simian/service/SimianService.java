package br.com.ml.simian.service;

import br.com.ml.simian.exception.SimianException;
import br.com.ml.simian.model.SimianStats;


public interface SimianService {

    boolean isSimian(String[] dna) throws SimianException;

    SimianStats calculateStats();
}
