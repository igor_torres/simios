package br.com.ml.simian.service;

import br.com.ml.simian.exception.SimianException;
import br.com.ml.simian.model.Simian;
import br.com.ml.simian.model.SimianStats;
import br.com.ml.simian.repository.SimianRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class SimianServiceDefault implements SimianService{


    SimianRepository simianRepository;
    @Autowired
    public SimianServiceDefault(SimianRepository simianRepository) {
        this.simianRepository = simianRepository;
    }

    @Override
    public boolean isSimian(String[] dna) throws SimianException {
        List<String> dnaList = Arrays.asList(dna);
        if(isSquareMatrix(dnaList) && isNitrogenousBase(dnaList)){
            Map<String, Boolean> horizontal = dnaList.stream().collect(Collectors.toMap(Function.identity(), this::seqGene, (existing, replacement) -> replacement));
            Map<String, Boolean> vertical = invert(dnaList).stream().collect(Collectors.toMap(Function.identity(), this::seqGene, (existing, replacement) -> replacement));
            Map<String, Boolean> diagonal = diagonal(dnaList).stream().collect(Collectors.toMap(Function.identity(), this::seqGene, (existing, replacement) -> replacement));
            Boolean isSimian = mergeAndVerify(horizontal, vertical, diagonal);
            Simian simian =  Simian.builder().dna(dnaList).horizontal(horizontal).vertical(vertical).diagonal(diagonal).isSimian(isSimian.toString()).build();
            simianRepository.save(simian);
            return isSimian;
        }else{
            throw new SimianException();
        }
    }

    @Override
    public SimianStats calculateStats() {

        Integer simian = simianRepository.count(Simian.builder().isSimian(String.valueOf(true)).build());
        Integer human = simianRepository.count(Simian.builder().isSimian(String.valueOf(false)).build());
        Double result = Double.parseDouble(simian.toString()) / Double.parseDouble(human.toString());
        return new SimianStats(simian, human, result);

    }

    public boolean mergeAndVerify(Map... map){
        List<Map<String, Boolean>> mapList = Arrays.asList(map);
        return mapList.stream().map(v -> v.entrySet().stream().map(m -> m.getValue()).collect(Collectors.toList())).flatMap(Collection::stream).filter(f -> f == true).findFirst().orElse(false);
    }

    public List<String> invert(List<String> dna){
        return IntStream.range(0, dna.size()).mapToObj(i -> dna.stream()
                                                            .map(d -> d.charAt(i))
                                                            .map(String::valueOf)
                                                            .collect(Collectors.joining())).collect(Collectors.toList());
    }

    public List<String> diagonal(List<String> dna){

        int run = dna.size() - 4;

        LinkedList<String> linkedDna = new LinkedList<>(dna);
        LinkedList<String> vert = new LinkedList<>(invert(dna));

        List<String> finalDiag = new ArrayList<>();
        String first = findDiagonal(linkedDna, () -> 1);
        finalDiag.add(first);

        List<String> down = IntStream.range(0, run).mapToObj(i -> findDiagonal(linkedDna, linkedDna::pop)).collect(Collectors.toList());
        finalDiag.addAll(down);

        List<String> up = IntStream.range(0, run).mapToObj(i -> findDiagonal(vert, vert::pop)).collect(Collectors.toList());
        finalDiag.addAll(up);


        LinkedList<String> linkedDnaReverse = new LinkedList<>(dna);
        Collections.reverse(linkedDnaReverse);
        LinkedList<String> vertReverse = new LinkedList<>(invert(new ArrayList<>(linkedDnaReverse)));

        String firstReverse = findDiagonal(linkedDnaReverse, () -> 1);
        finalDiag.add(firstReverse);

        List<String> downReverse = IntStream.range(0, run).mapToObj(i -> findDiagonal(linkedDnaReverse, linkedDnaReverse::pop)).collect(Collectors.toList());
        finalDiag.addAll(downReverse);

        List<String> upReverse = IntStream.range(0, run).mapToObj(i -> findDiagonal(vertReverse, vertReverse::pop)).collect(Collectors.toList());
        finalDiag.addAll(upReverse);

        return finalDiag;

    }


    public String findDiagonal(LinkedList<String> list, Supplier supplier) {
        supplier.get();
        LinkedList<String> rebuild = new LinkedList<>(invert(new ArrayList<>(list)));
        return IntStream.range(0, list.size()).filter(i -> list.get(i).charAt(i) == rebuild.get(i).charAt(i)).mapToObj(j -> list.get(j).charAt(j)).map(String::valueOf).collect(Collectors.joining());
    }


    public boolean isSquareMatrix(List<String> dna){
        return !dna.stream().anyMatch(s -> s.chars().count() != dna.size());
    }

    public boolean isNitrogenousBase(List<String> dna){
        return dna.stream()
                .flatMap(s -> s.codePoints().mapToObj(Character::toChars))
                .map(String::valueOf)
                .noneMatch(f -> !(f.equals("A") || f.equals("T") || f.equals("C") || f.equals("G")));
    }

    public Boolean seqGene(String nitro){
        boolean a = nitro.contains("AAAA");
        boolean c = nitro.contains("CCCC");
        boolean t = nitro.contains("TTTT");
        boolean g = nitro.contains("GGGG");
        if(a || c || t || g){
            return true;
        }else{
            return false;
        }
    }
}
