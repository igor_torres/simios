package br.com.ml.simian.swagger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final String TAG_NAME = "simian";


    /**
     * Método responsável por realizar a instanciação do Bean de configuração do
     * Swagger. para business
     *
     * @return {@link Docket} - Configuração do Swagger
     */
    @Bean
    public Docket swaggerSpringBusinessView() {
        return new Docket(DocumentationType.SWAGGER_2)
                .tags(
                        new Tag(TAG_NAME, "Representa os metodos para o Simian.")
                )
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.ml.simian.controller"))
                .paths(paths())
                .build()
                .apiInfo(this.metaData())
                .useDefaultResponseMessages(false);
    }

    private Predicate<String> paths() {
        return Predicates.not(PathSelectors.regex("/v1/admin/.*"));
    }

    /**
     * Método responsável por instanciar o Bean de informações da API.
     *
     * @return {@link ApiInfo} - Classe de informações da API
     */
    private ApiInfo metaData() {
        return new ApiInfoBuilder().title("Microsserviço Simian").description(
                "Microsserviço responsável por realizar a validação e status de um simian ")
                .version("v1")
                .contact(new Contact("Simian", "https://bitbucket.org/", "igor.v.torres@gmail.com")).build();
    }


}