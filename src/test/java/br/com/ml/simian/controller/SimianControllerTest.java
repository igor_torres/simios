package br.com.ml.simian.controller;

import br.com.ml.simian.exception.SimianException;
import br.com.ml.simian.service.SimianServiceDefault;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest({SimianController.class})
public class SimianControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SimianServiceDefault simianService;

    @Test
    public void post_simian_200() throws Exception {
        final String MEDIA_TYPE_CAMPO = "Content-Type";
        final String MEDIA_TYPE_VALOR = "application/json";

        Mockito.when(this.simianService.isSimian(Mockito.any()))
                .thenReturn(true);

        this.mockMvc.perform(
                post("/simian")
                        .header(MEDIA_TYPE_CAMPO, MEDIA_TYPE_VALOR)
                        .content("{\"dna\":[\"CT\", \"GA\"]}"))
                .andExpect(status().isOk());
    }

    @Test
    public void post_simian_403() throws Exception {
        final String MEDIA_TYPE_CAMPO = "Content-Type";
        final String MEDIA_TYPE_VALOR = "application/json";

        Mockito.when(this.simianService.isSimian(Mockito.any()))
                .thenReturn(false);

        this.mockMvc.perform(
                post("/simian")
                        .header(MEDIA_TYPE_CAMPO, MEDIA_TYPE_VALOR)
                        .content("{\"dna\":[\"CT\", \"GA\"]}"))
                .andExpect(status().isForbidden());
    }
    @Test
    public void post_simian_400() throws Exception {
        final String MEDIA_TYPE_CAMPO = "Content-Type";
        final String MEDIA_TYPE_VALOR = "application/json";

        Mockito.when(this.simianService.isSimian(Mockito.any()))
                .thenThrow(new SimianException());

        this.mockMvc.perform(
                post("/simian")
                        .header(MEDIA_TYPE_CAMPO, MEDIA_TYPE_VALOR)
                        .content("{\"dn\":[\"T\", \"GF\"]}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_simian_stats() throws Exception {
        final String MEDIA_TYPE_CAMPO = "Content-Type";
        final String MEDIA_TYPE_VALOR = "application/json";

        Mockito.when(this.simianService.isSimian(Mockito.any()))
                .thenReturn(true);

        this.mockMvc.perform(
                get("/stats")
                        .header(MEDIA_TYPE_CAMPO, MEDIA_TYPE_VALOR))
                        .andExpect(status().isOk());
    }
}
