package br.com.ml.simian.service;

import br.com.ml.simian.exception.SimianException;
import br.com.ml.simian.model.SimianStats;
import br.com.ml.simian.repository.SimianRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

public class SimianServiceDefaultTest {


    SimianRepository simianRepository = Mockito.mock(SimianRepository.class);

    @Test
    public void should_be_nitrogenous(){
        List<String> dna = Arrays.asList("CTGAGA", "CTGAGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG");
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        boolean result = simianServiceDefault.isNitrogenousBase(dna);
        Assert.assertTrue(result);
    }
    @Test
    public void should_not_be_nitrogenous(){
        List<String> dna = Arrays.asList("CTGAGA", "CTXAGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG");
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        boolean result = simianServiceDefault.isNitrogenousBase(dna);
        Assert.assertFalse(result);
    }

    @Test
    public void should_return_false_when_matrix_NxN(){
        List<String> dna = Arrays.asList("CTGAGA", "CTGAG", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG");
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        boolean result = simianServiceDefault.isSquareMatrix(dna);
        Assert.assertFalse(result);
    }


    @Test
    public void should_invert_matrix(){
        List<String> expected = Arrays.asList("CCTACT", "TTAGCC", "GATACA", "ATTGCC", "GGGGTT", "ACTGAG");
        List<String> dna = Arrays.asList("CTGAGA", "CTATGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG");
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        List<String> horizontal = simianServiceDefault.invert(dna);
        Assert.assertEquals(expected, horizontal);
    }
    @Test
    public void diagonalTest(){
        List<String> expected = Arrays.asList("CTTGTG", "CAACT", "TGCC", "TATGA", "GTGG", "TCATGA", "CGTTG", "AAAA", "CCGGC", "ACGT");
        List<String> dna = Arrays.asList("CTGAGA", "CTATGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG");
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        List<String> result = simianServiceDefault.diagonal(dna);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void should_be_simian(){
        List<String> dna2 = Arrays.asList("CTGAGA", "CTGAGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG");
        Object[] objArr = dna2.toArray();
        String[] dnaArray = Arrays.copyOf(objArr, objArr.length, String[].class);
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        try{
            boolean result = simianServiceDefault.isSimian(dnaArray);
            Assert.assertTrue(result);
        }catch(Exception e){
            Assert.fail();
        }

    }

    @Test
    public void should_not_be_simian(){
        List<String> dna2 = Arrays.asList("CT", "AG");
        Object[] objArr = dna2.toArray();
        String[] dnaArray = Arrays.copyOf(objArr, objArr.length, String[].class);
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        try{
            boolean result = simianServiceDefault.isSimian(dnaArray);
            Assert.assertFalse(result);
        }catch(Exception e){
            Assert.fail();
        }

    }

    @Test(expected = SimianException.class)
    public void should_not_be_matrix() throws SimianException {
        List<String> dna2 = Arrays.asList("CT", "A");
        Object[] objArr = dna2.toArray();
        String[] dnaArray = Arrays.copyOf(objArr, objArr.length, String[].class);
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        boolean result = simianServiceDefault.isSimian(dnaArray);
    }



    @Test
    public void should_get_stats(){
        SimianServiceDefault simianServiceDefault = new SimianServiceDefault(simianRepository);
        Mockito.when(simianRepository.count(Mockito.any())).thenReturn(2);
        SimianStats result = simianServiceDefault.calculateStats();
        Assert.assertEquals(2, result.getCount_human_dna().intValue());
        Assert.assertEquals(2, result.getCount_mutant_dna().intValue());
        Assert.assertEquals(1.0, result.getRatio().doubleValue(), 0);

    }

}
